@extends('layouts.main')

@section('container')
<div class="container mt-4">
    <div class="row mb-4 g-0 justify-content-center">
        <!-- Featured blog post-->
        @if ($data->currentPage() == 1)
        @foreach ($jumbotron as $news)
        <div class="col-md-6">
            <a href="/news/{{ $news->slug }}">
                <div class="card overflow-hidden">
                    <img src="https://source.unsplash.com/558x300?{{ $news->category->category }}" class="card-img-news"
                        alt="Hot News">
                    <div class="card-img-overlay rounded-0 rounded-bottom text-white"
                        style="background-color: rgba(0, 0, 0, 0.5)">
                        <h5 class="card-title">{{ $news->title }}</h5>
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.</p>
                        <p class="card-text">Last updated at {{ $news->updated_at->diffForHumans() }}</p>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
        @endif
    </div>
</div>

<div class="container">
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                @foreach ($data as $news)
                <div class="col-lg-6">
                    <!-- Blog post-->
                    <a href="/news/{{ $news->slug }}" class="text-decoration-none text-black">
                        <div class="card-news mb-4 border-0">
                            <div class="overflow-hidden">
                                <img class="card-img-news"
                                    src="https://source.unsplash.com/555x300?{{ $news->category->category }}"
                                    alt="{{ $news->category->category }}" />
                            </div>
                            <div class="card-body-news p-0 mt-2">
                                <h2 class="card-title h4">{{ $news->title }}</h2>
                                <div class="small text-muted">{{ date('F d, Y', strtotime($news->created_at)) }}</div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            {{ $data->links() }}
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="#!">Web Design</a></li>
                                <li><a href="#!">HTML</a></li>
                                <li><a href="#!">Freebies</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="#!">JavaScript</a></li>
                                <li><a href="#!">CSS</a></li>
                                <li><a href="#!">Tutorials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Side widget-->
            <div class="card mb-4">
                <div class="card-header">Side Widget</div>
                <div class="card-body">You can put anything you want inside of these side widgets. They are easy to
                    use, and feature the Bootstrap 5 card component!</div>
            </div>
        </div>
    </div>
</div>
@endsection