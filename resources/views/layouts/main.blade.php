<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    @if (Request::is('/'))
    <title>BK News | Berita Terbaru, Aduan Suara Mahasiswa</title>
    @else
    <title>@yield('title') - BK News</title>
    @endif

    <!-- Font Awesome v6 -->
    <script src="{{ url('https://kit.fontawesome.com/eedb1855ec.js') }}" crossorigin="anonymous"></script>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="{{ url('favicon.ico') }}" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ url('css/styles.css') }}" rel="stylesheet" />
</head>

<body>
    <!-- Responsive navbar-->
    @include('layouts.navbar')

    <!-- Page content-->
    @yield('container')


    <!-- Footer-->
    <footer class="py-3 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; 2022 Made with <i
                    class="fa-solid fa-heart text-danger"></i> By <a href="https://www.instagram.com/banakhoesnan"
                    class="text-decoration-none text-white" target="_blank">Bana
                    Khusnan</a></p>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="{{ url('https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Core theme JS-->
    <script src="{{ url('js/scripts.js') }}"></script>
</body>

</html>