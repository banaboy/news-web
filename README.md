<p align="center"><img src="public/img/logo-lpm.png" height="100"></p>

## BK News

BK News (Bana Khusnan News) adalah suatu media berisikan berita dan tempat aduan bagi mahasiswa yang merasa resah dengan keadaan kampus. Website ini tidak hanya berisikan berita, namun dapat mengadukan suara yang langsung dikirim ke email layanan kampus, agar kampus segera menindak lanjuti, dan aduan tersebut yang telah dikonfirmasi oleh admin maka akan dijadikan bahan berita di website ini.

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200"></a></p>

## Laravel

Laravel adalah framework aplikasi web dengan sintaks yang ekspresif dan elegan. Dengan basis bahasa pemrograman yaitu PHP (Hypertext Prepocessor).

###### Mengapa Laravel?
Kami menggunakan Laravel, karena menurut saya dokumentasi dari Laravel cukup lengkap dan cukup jelas. Mudah digunakan dan mudah dipahami. Sekaligus sebagai portfolio atau project pertama kami dalam membuat suatu website. Ada beberapa hal yang kami pelajari, seperti:

- Autentikasi dan Autorisasi (Menggunakan JWT Autentikasi)
- Send Mail
- Rest API
- Middleware