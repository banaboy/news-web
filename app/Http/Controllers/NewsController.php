<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    public function index(){
        $jumbotron = News::paginate(2);
        $data = News::paginate(2)->withQueryString();

        return view('news.index', [
            "jumbotron" => $jumbotron,
            "data" => $data
        ]);
    }

    public function detailNews(News $slug){
        $data = News::all();
        return view('news.detail', [
            "news" => $slug,
            "otherNews" => $data
        ]);
    }

    public function about()
    {
        return view('about.index');
    }
}
