<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\News;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(3)->create();

        Category::create([
            'slug' => 'sport',
            'category' => 'Sport'
        ]);

        Category::create([
            'slug' => 'government',
            'category' => 'Government'
        ]);

        Category::create([
            'slug' => 'agriculture',
            'category' => 'Agriculture'
        ]);
        News::factory(3)->create();
    }
}
